# Devin Sylva, Sr. Site Reliability Engineer

This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before.

Please feel free to contribute to this page by opening a merge request.

## About me

- Musician (Mostly Guitar and Music Production)
- Tango Dancer
- Homesteader
- Jungle Dweller
- Ham Radio Oprator
- Maker

## How you can help me

- If I have a technical question for you: Assume that I have the background info to understand your answer and get right to the point. If I don't have a pre-requisite piece of information, I'll ask or go find it.
- Feel free to answer any question I have with a URL - I won't think it's rude at all, and will appreciate the efficient transfer of information.
-

## My working style

I alternate between highly focused work where distractions are unwelcome, and high level work where I look for as much input as possible. If I am not responding quickly, it is likely that I am in a focus phase.

## What I assume about others

- I assume that others know all about the subject we are discussing.  Please speak up if I assume that you know something that you don't.  I am happy to adjust my communication.
- I assume that others have good intent and are working towards the same goals that I am.  If you are being malicious or are trying to sabotage me, please be extremely obvious about it if you want me to notice.

## What I want to learn

- I want to learn more ways to engineer high availability
- I want to learn how to work around the current limitations of the tools we all use
- I want to learn about new technologies coming out that will change how we do things
- I want to learn about other people's workflows and how I can incorporate good ideas into mine

## Communicating with me

- I prefer Slack for quick communication. Please feel free to ping me about anything.  Keep in mind that my time zone is HAST (GMT-10), and I rarely respond to non-emergency pings outside of business hours.  If it's not urgent, I will get back to you in the morning.
- During working hours, I am always happy to jump on a Zoom call - whether it's for a quick question that would be hard to type out in Slack, or just a social call.
- Feel free to put a coffee chat on my calendar anyplace you can find space.  I'll suggest another time if it's inconvenient.  I'm happy to talk about Hawaii, Music, Tech, or anything really.

## Related pages

- [The Consul Outage that Never Happened](https://about.gitlab.com/blog/2019/11/08/the-consul-outage-that-never-happened/)
